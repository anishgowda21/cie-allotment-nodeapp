const mongoose = require("mongoose");

const semesterSchema = mongoose.Schema({
  semester: {
    type: Number,
    required: true,
  },
  no_of_subjects: {
    type: Number,
    default: 0,
  },
  no_of_rooms_available: {
    type: Number,
    default: 0,
  },
  available_rooms: {
    type: Array,
    default: [],
  },
  total_duites: {
    type: Number,
    default: 0,
  },
  subjects: [
    {
      _id: {
        type: mongoose.Types.ObjectId,
        required: true,
      },
      code: {
        type: String,
        required: true,
      },
      name: {
        type: String,
        required: true,
      },
      exam_date: {
        type: String,
        required: true,
      },
      exam_start_time: {
        type: String,
        required: true,
      },
      exam_end_time: {
        type: String,
        required: true,
      },
      fixed_faculty: {
        type: Array,
      },
    },
  ],
});

module.exports = mongoose.model("Semester", semesterSchema);
