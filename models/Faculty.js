const mongoose = require("mongoose");
const facultySchema = mongoose.Schema({
  faculty_id: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  phone_number: {
    type: String,
    required: true,
  },
  designation: {
    type: String,
    required: true,
  },
  duties_assigned: {
    type: Number,
    default: 0,
  },
  password: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("Faculty", facultySchema);
