const express = require("express");
const nunjucks = require("nunjucks");
const path = require("path");
const dotenv = require("dotenv").config();
const flash = require("connect-flash");
const ConnectDB = require("./config/db");
const app = express();

// Conntect to MongoDB
ConnectDB();

// Configure Nunjucks
app.engine("views", nunjucks.render);
app.set("view engine", "html");
nunjucks.configure("views", {
  autoescape: true,
  express: app,
  watch: true,
});

// Static folder for CSS, JS, Images
app.use(express.static(path.join(__dirname, "static")));

// Body parser
app.use(express.urlencoded({ extended: false }));

// Routes
app.use("/", require("./routes/index"));
app.use("/timeTable/", require("./routes/timeTable"));
app.use("/faculty/", require("./routes/faculty"));

const port = process.env.PORT || 5000;

app.listen(port, console.log(`App Started on port ${port}`));
