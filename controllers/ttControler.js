const Semester = require("../models/Semester");
var mongoose = require("mongoose");

function getSemlist(sem = -1) {
  var sem_list = ["1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th"];
  if (sem != -1) {
    const index = sem_list.indexOf(sem);
    sem_list.splice(index, 1);
  }
  return sem_list;
}

const showSelection = async (req, res) => {
  const sem_list = getSemlist();
  res.render("time_table.html", { sem_list: sem_list });
};

const getTimeTable = async (req, res) => {
  const select_sem = req.query.select_sem;

  if (!select_sem || !getSemlist().includes(select_sem)) {
    return res.redirect("/timeTable");
  }

  var sem = select_sem.charAt(0);
  let subjects = null;
  try {
    const semesterCollec = await Semester.findOne({ semester: sem });
    if (semesterCollec) {
      subjects = semesterCollec.subjects;
    }

    res.render("time_table.html", {
      data: subjects,
      sem: select_sem,
      sem_list: getSemlist(select_sem),
    });
  } catch (error) {
    console.log(error);
  }
};

const insertTimeTable = async (req, res) => {
  const {
    semester,
    courseCode,
    courseName,
    examDate,
    examStartTime,
    examEndTime,
    fixedFaculties,
  } = req.body;

  if (
    !semester ||
    !courseCode ||
    !courseName ||
    !examDate ||
    !examStartTime ||
    !examEndTime
  ) {
    return res.redirect("/timeTable");
  }

  var sem = semester.charAt(0);
  if (fixedFaculties) {
    var fixedArray = fixedFaculties.split(",").filter((x) => x);
  } else {
    var fixedArray = [];
  }
  var subject = {
    _id: new mongoose.Types.ObjectId(),
    code: courseCode,
    name: courseName,
    exam_date: examDate,
    exam_start_time: examStartTime,
    exam_end_time: examEndTime,
    fixed_faculty: fixedArray,
  };
  try {
    // Find and update the correct timetable
    const timeTable = await Semester.findOneAndUpdate(
      { semester: sem },
      { $push: { subjects: subject } }
    );
    if (!timeTable) {
      // Create new timetable
      const newTimeTable = new Semester({
        semester: sem,
        subjects: [subject],
      });
      await newTimeTable.save();
    }
    res.redirect("/timeTable/getTimeTable?select_sem=" + semester);
  } catch (error) {
    console.log(error);
  }
};

const deleteTimeTable = async (req, res) => {
  const { semester, rec_id } = req.body;

  if (!semester || !rec_id) {
    return res.redirect("/timeTable");
  }

  var sem = semester.charAt(0);
  try {
    // Find and update the correct timetable
    const timeTable = await Semester.findOneAndUpdate(
      { semester: sem },
      { $pull: { subjects: { _id: rec_id } } }
    );
    res.redirect("/timeTable/getTimeTable?select_sem=" + semester);
  } catch (error) {
    console.log(error);
  }
};

const updateTimeTable = async (req, res) => {
  const {
    sem_no,
    rec_id,
    courseCode,
    courseName,
    examDate,
    examStartTime,
    examEndTime,
    fixedFaculties,
  } = req.body;

  if (
    !sem_no ||
    !courseCode ||
    !courseName ||
    !examDate ||
    !examStartTime ||
    !examEndTime
  ) {
    return res.redirect("/timeTable");
  }
  var fixedArray = [];
  var sem = sem_no.charAt(0);
  if (fixedFaculties) {
    fixedArray = fixedFaculties.split(",").filter((x) => x);
  }
  var subject = {
    _id: rec_id,
    code: courseCode,
    name: courseName,
    exam_date: examDate,
    exam_start_time: examStartTime,
    exam_end_time: examEndTime,
    fixed_faculty: fixedArray,
  };

  try {
    const timeTable = await Semester.findOneAndUpdate(
      { semester: sem, "subjects._id": rec_id },
      { $set: { "subjects.$": subject } }
    );
    res.redirect("/timeTable/getTimeTable?select_sem=" + sem_no);
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  getTimeTable,
  showSelection,
  insertTimeTable,
  deleteTimeTable,
  updateTimeTable,
};
