const Faculty = require("../models/Faculty");
const bcrypt = require("bcryptjs");
const mongoose = require("mongoose");

const showFacultyTable = async (req, res) => {
  try {
    const faculty = await Faculty.find();
    res.render("faculty.html", { data: faculty });
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
};

const insertFaculty = async (req, res) => {
  const {
    faculty_id,
    faculty_name,
    faculty_email,
    faculty_phone,
    faculty_designation,
  } = req.body;

  if (
    !faculty_id ||
    !faculty_name ||
    !faculty_email ||
    !faculty_phone ||
    !faculty_designation
  ) {
    return res.redirect("/faculty");
  }
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(faculty_email, salt);

  var faculty = {
    faculty_id: faculty_id,
    name: faculty_name,
    email: faculty_email,
    phone_number: faculty_phone,
    designation: faculty_designation,
    duties_assigned: 0,
    password: hashedPassword,
  };
  const newFaculty = await Faculty.create(faculty);

  res.send(newFaculty);
};

const deleteFaculty = async (req, res) => {
  const { faculty_id } = req.body;
  console.log(faculty_id);
  try {
    const faculty = await Faculty.findById(faculty_id);
    await faculty.remove();
    res.redirect("/faculty");
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
};

const updateFaculty = async (req, res) => {
  console.log(req.body);
  const {
    rec_id,
    faculty_id,
    faculty_name,
    faculty_email,
    faculty_phone,
    faculty_designation,
    faculty_da,
  } = req.body;
  try {
    // Update the existing record with the new values
    const faculty = await Faculty.findOneAndUpdate(
      { _id: rec_id },
      {
        faculty_id: faculty_id,
        name: faculty_name,
        email: faculty_email,
        phone_number: faculty_phone,
        designation: faculty_designation,
      }
    );
    return res.redirect("/faculty");
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
};

module.exports = {
  showFacultyTable,
  insertFaculty,
  deleteFaculty,
  updateFaculty,
};
