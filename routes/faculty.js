const express = require("express");
const router = express.Router();
const {
  showFacultyTable,
  insertFaculty,
  deleteFaculty,
  updateFaculty,
} = require("../controllers/facultyController");

router.get("/", showFacultyTable);
router.post("/insertFaculty", insertFaculty);
router.post("/deleteFaculty", deleteFaculty);
router.post("/updateFaculty", updateFaculty);

module.exports = router;
