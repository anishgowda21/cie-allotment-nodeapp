const express = require("express");
const router = express.Router();
const {
  getTimeTable,
  showSelection,
  insertTimeTable,
  deleteTimeTable,
  updateTimeTable,
} = require("../controllers/ttControler");
router.get("/", showSelection);
router.get("/getTimeTable", getTimeTable);
router.post("/insertTimeTable", insertTimeTable);
router.post("/deleteTimeTable", deleteTimeTable);
router.post("/updateTimeTable", updateTimeTable);
module.exports = router;
