const express = require("express");
const router = express.Router();

router.get("/", (req, res) => res.render("index.html"));
router.get("/dashboard", (req, res) => res.render("dashboard.html"));

module.exports = router;
